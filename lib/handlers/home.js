'use strict';

exports.home = {
    auth: {
        mode: 'try'
    },
    handler: function (request, reply) {

        return reply.view('home/home');
    }
};
