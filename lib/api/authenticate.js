'use strict';

const R = require('./request');

module.exports = function (email, password, cb) {

    return R('/me', (err, res, body) => {

        if (err) {
            return cb(err);
        }

        if (res.statusCode !== 200) {
            return cb(new Error(body));
        }

        return cb(null, body);
    }).auth(email, password, true);
};
