'use strict';

const Inert = require('inert');
const Vision = require('vision');
const HapiAuthCookie = require('hapi-auth-cookie');
const Handlebars = require('handlebars');
const Good = require('good');
const Crumb = require('crumb');
const Path = require('path');
const Cryptiles = require('cryptiles');

const Routes = require('./routes');

exports.register = function (server, options, next) {

    const cache = server.cache({ segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000 });
    server.app.cache = cache;

    server.register([
        Vision,
        Inert,
        HapiAuthCookie,
        {
            register: Crumb,
            options: {
                cookieOptions: {
                    isSecure: false
                }
            }
        },
        {
            register: Good,
            options: {
                reporters: {
                    console: [{
                        module: 'good-squeeze',
                        name: 'Squeeze',
                        args: [{
                            response: '*',
                            log: '*'
                        }]
                    }, {
                        module: 'good-console'
                    }, 'stdout']
                }
            }
        }
    ], (err) => {

        if (err) {
            return next(err);
        }

        server.auth.strategy('session', 'cookie', true, {
            password: Cryptiles.randomString(32),
            redirectTo: '/login',
            redirectOnTry: false,
            isSecure: false,
            appendNext: true,
            validateFunc: function (request, session, callback) {

                cache.get(session.sid, (err, cached) => {

                    if (err) {
                        return callback(err, false);
                    }

                    if (!cached) {
                        return callback(null, false);
                    }

                    return callback(null, true, cached.account);
                });
            }
        });

        server.route({
            method: 'GET',
            path: '/{param*}',
            config: {
                auth: false,
                files: {
                    relativeTo: Path.resolve(__dirname, 'public', 'dist')
                }
            },
            handler: {
                directory: {
                    path: '.',
                    redirectToSlash: true
                }
            }
        });

        server.views({
            engines: { hbs: Handlebars },
            relativeTo: Path.resolve(__dirname, 'templates'),
            layout: true,
            layoutPath: 'layout',
            partialsPath: 'partials',
            helpersPath: 'helpers'
        });

        server.route(Routes.endpoints);

        return next();
    });
};

exports.register.attributes = {
    pkg: require('../package.json')
};
