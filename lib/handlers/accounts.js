'use strict';

exports.new = {
    auth: {
        mode: 'try'
    },
    handler: function (request, reply) { }
};

exports.create = {
    auth: false,
    handler: function (request, reply) { }
};

exports.index = {
    handler: function (request, reply) { }
};

exports.destroy = {
    handler: function (request, reply) { }
};

exports.register = {
    auth: false,
    handler: function (request, reply) { }
};
