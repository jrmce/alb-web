'use strict';

const Hapi = require('hapi');

const Web = require('.');

const server = new Hapi.Server();
server.connection({ port: 8000, host: 'localhost' });

server.register([Web], (err) => {

    if (err) {
        throw err;
    }

    server.start(() => {

        server.log('info', 'Web Started');
    });
});
