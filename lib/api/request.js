'use strict';

const Request = require('request');

module.exports = Request.defaults({
    baseUrl: 'http://localhost:8001',
    json: true
});
