'use strict';

const Uuid = require('node-uuid');

const Api = require('../api');

exports.new = {
    auth: {
        mode: 'try'
    },
    handler: function (request, reply) {

        if (request.auth.credentials) {
            return reply.redirect('/');
        }

        return reply.view('sessions/new');
    }
};

exports.create = {
    auth: false,
    handler: function (request, reply) {

        return Api.authenticate(request.payload.email, request.payload.password, (err, account) => {

            if (err) {
                return reply.redirect('/sessions/new');
            }

            const sid = Uuid.v4();
            request.server.app.cache.set(sid, { account }, 0, (err) => {

                if (err) {
                    return reply(err);
                }

                request.cookieAuth.set({ sid });

                return reply.redirect('/');
            });
        });
    }
};

exports.index = {
    handler: function (request, reply) { }
};

exports.destroy = {
    handler: function (request, reply) { }
};

exports.login = {
    auth: false,
    handler: function (request, reply) { }
};

exports.logout = {
    auth: false,
    handler: function (request, reply) { }
};
