'use strict';

const Home = require('./handlers/home');
const Accounts = require('./handlers/accounts');
const Sessions = require('./handlers/sessions');

exports.endpoints = [
    // Home
    { method: 'GET', path: '/', config: Home.home },

    // Accounts
    { method: 'GET', path: '/accounts/new', config: Accounts.new },
    { method: 'GET', path: '/accounts/destroy', config: Accounts.destroy },
    { method: 'POST', path: '/accounts', config: Accounts.create },
    { method: 'GET', path: '/accounts', config: Accounts.index },
    { method: 'GET', path: '/register', config: Accounts.register },

    // Sessions
    { method: 'GET', path: '/sessions/new', config: Sessions.new },
    { method: 'GET', path: '/sessions/destroy', config: Sessions.destroy },
    { method: 'POST', path: '/sessions', config: Sessions.create },
    { method: 'GET', path: '/login', config: Sessions.login },
    { method: 'GET', path: '/logout', config: Sessions.logout }
];
